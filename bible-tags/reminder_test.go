package bibletags

import (
	"testing"
	"time"

	"gitlab.com/martyros/sqlutil/types/timedb"
)

func parseTimeMust(value string) timedb.Time {
	tm, err := time.Parse(time.RFC3339, value)
	if err != nil {
		panic(err)
	}

	return timedb.Time{Time: tm}
}

func parseTzMust(tzstring string) *time.Location {
	loc, err := time.LoadLocation(tzstring)
	if err != nil {
		panic(err)
	}

	return loc
}

func TestTimerMachinery(t *testing.T) {
	btdb, err := OpenDb(testTimerMachinery)
	if err != nil {
		t.Errorf("Opening mock DB for testing: %v", err)
		return
	}

	rchan, err := btdb.OpenReminderChannel()
	if err != nil {
		t.Errorf("Getting reminder channel: %v", err)
		return
	}

	done := make(chan struct{})

	go func() {
		for reminder := range rchan {
			t.Logf("Got reminder %v", reminder)
		}

		t.Logf("rchan closed, closing done channel")
		close(done)
	}()

	time.Sleep(time.Millisecond * 500)
	btdb.reminderCommand <- reminderCmdRescan
	time.Sleep(time.Millisecond * 500)
	btdb.reminderCommand <- reminderCmdRescan

	time.Sleep(time.Second)

	btdb.CloseReminderChannel()

	t.Logf("Waiting for reminder goroutine to finish")
	<-done
}

func TestDayspec(t *testing.T) {
	tests := []struct {
		dbnow   string
		dayspec string
		valid   bool
		match   bool
	}{
		{"2022-07-03T12:00:00Z", "daily", true, true},
		{"2022-07-04T12:00:00Z", "daily", true, true},
		{"2022-07-05T12:00:00Z", "daily", true, true},
		{"2022-07-06T12:00:00Z", "daily", true, true},
		{"2022-07-07T12:00:00Z", "daily", true, true},
		{"2022-07-08T12:00:00Z", "daily", true, true},
		{"2022-07-09T12:00:00Z", "daily", true, true},
		{"2022-07-03T12:00:00Z", "sunday", true, true},
		{"2022-07-04T12:00:00Z", "sunday", true, false},
		{"2022-07-05T12:00:00Z", "sunday", true, false},
		{"2022-07-06T12:00:00Z", "sunday", true, false},
		{"2022-07-07T12:00:00Z", "sunday", true, false},
		{"2022-07-08T12:00:00Z", "sunday", true, false},
		{"2022-07-09T12:00:00Z", "sunday", true, false},
		{"2022-07-03T12:00:00Z", "monday", true, false},
		{"2022-07-04T12:00:00Z", "monday", true, true},
		{"2022-07-05T12:00:00Z", "monday", true, false},
		{"2022-07-06T12:00:00Z", "monday", true, false},
		{"2022-07-07T12:00:00Z", "monday", true, false},
		{"2022-07-08T12:00:00Z", "monday", true, false},
		{"2022-07-09T12:00:00Z", "monday", true, false},
		{"2022-07-04T12:00:00Z", "Monday", true, true},
		{"2022-07-03T12:00:00Z", "tuesday", true, false},
		{"2022-07-04T12:00:00Z", "tuesday", true, false},
		{"2022-07-05T12:00:00Z", "tuesday", true, true},
		{"2022-07-06T12:00:00Z", "tuesday", true, false},
		{"2022-07-07T12:00:00Z", "tuesday", true, false},
		{"2022-07-08T12:00:00Z", "tuesday", true, false},
		{"2022-07-09T12:00:00Z", "tuesday", true, false},
		{"2022-07-03T12:00:00Z", "weekdays", true, false},
		{"2022-07-04T12:00:00Z", "weekdays", true, true},
		{"2022-07-05T12:00:00Z", "weekdays", true, true},
		{"2022-07-06T12:00:00Z", "weekdays", true, true},
		{"2022-07-07T12:00:00Z", "weekdays", true, true},
		{"2022-07-08T12:00:00Z", "weekdays", true, true},
		{"2022-07-09T12:00:00Z", "weekdays", true, false},
		{"2022-07-03T12:00:00Z", "tuesday,thursday", true, false},
		{"2022-07-04T12:00:00Z", "tuesday,thursday", true, false},
		{"2022-07-05T12:00:00Z", "tuesday,thursday", true, true},
		{"2022-07-06T12:00:00Z", "tuesday,thursday", true, false},
		{"2022-07-07T12:00:00Z", "tuesday,thursday", true, true},
		{"2022-07-08T12:00:00Z", "tuesday,thursday", true, false},
		{"2022-07-09T12:00:00Z", "tuesday,thursday", true, false},

		{"2022-07-03T12:00:00Z", "weekends", true, true},
		{"2022-07-04T12:00:00Z", "weekends", true, false},
		{"2022-07-05T12:00:00Z", "weekends", true, false},
		{"2022-07-06T12:00:00Z", "weekends", true, false},
		{"2022-07-07T12:00:00Z", "weekends", true, false},
		{"2022-07-08T12:00:00Z", "weekends", true, false},
		{"2022-07-09T12:00:00Z", "weekends", true, true},

		{"2022-07-03T12:00:00Z", "weekends,friday", true, true},
		{"2022-07-04T12:00:00Z", "weekends,friday", true, false},
		{"2022-07-05T12:00:00Z", "weekends,friday", true, false},
		{"2022-07-06T12:00:00Z", "weekends,friday", true, false},
		{"2022-07-07T12:00:00Z", "weekends,friday", true, false},
		{"2022-07-08T12:00:00Z", "weekends,friday", true, true},
		{"2022-07-09T12:00:00Z", "weekends,friday", true, true},
	}

	for _, test := range tests {
		tm := parseTimeMust(test.dbnow)
		t.Logf("Testing %s (weekday %s) against %s",
			test.dbnow, tm.Weekday().String(), test.dayspec)
		days, err := dayspecParse(test.dayspec)
		if err != nil {
			if test.valid {
				t.Errorf("ERROR: Expected %s to parse, failed with %v",
					test.dayspec, err)
			}
			continue
		} else {
			if !test.valid {
				t.Errorf("ERROR: Expected %s to fail parse, succeded!",
					test.dayspec)
				continue
			}
		}
		if match := dayspecMatch(days, tm); match != test.match {
			t.Errorf("ERROR: Expected dayspecMatch to return %v, got %v",
				test.match, match)
		}
	}
}

// Test timeoutCalc
func TestTimeoutCalc(t *testing.T) {
	loc := timedb.Location{Location: time.UTC}

	tests := []struct {
		dbnow              string
		dayspec, dayoffset string
		expectedString     string
		iserror            bool
	}{
		{"2020-01-15T12:00:00Z", "daily", "13:00", "2020-01-15T13:00:00Z", false},
		{"2020-01-15T12:00:00Z", "daily", "11:15", "2020-01-16T11:15:00Z", false},
		{"2020-01-15T12:00:00Z", "daily", "1:15PM", "", true},
		{"2020-01-15T12:00:00Z", "monday", "11:15", "2020-01-20T11:15:00Z", false},
	}

	for _, test := range tests {
		got, err := timeoutCalc(parseTimeMust(test.dbnow), test.dayspec, test.dayoffset, loc)
		if err != nil {
			if !test.iserror {
				t.Errorf("Unexpected error for %v %s %s %v: %v",
					test.dbnow, test.dayspec, test.dayoffset, loc, err)
			}
			continue
		} else {
			if test.iserror {
				t.Errorf("Unexpected success for %v %s %s %v", test.dbnow, test.dayspec, test.dayoffset, loc)
				continue
			}

			if got.Format(time.RFC3339) != test.expectedString {
				t.Errorf("Got %s wanted %s", got.Format(time.RFC3339), test.expectedString)
			}
		}
	}
}

// Test set/list/delete/scan reminders
func TestReminderCore(t *testing.T) {
	btdb := getTestDb(t)

	dbnow := parseTimeMust("2020-01-15T12:00:00Z")
	btdb.testNow = &dbnow

	// First run ReminderScan on an empty DB to make sure we don't get an error
	if outstanding, timeout, err := btdb.RemiderScan(); err != nil {
		t.Errorf("Running reminderscan on an empty DB: %v", err)
	} else {
		if len(outstanding) != 0 {
			t.Errorf("Unexpected outstanding reminders %v", outstanding)
		}
		if timeout != btdb.defaultTimeout {
			t.Errorf("Unexpected timeout: got %v wanteg %v", timeout, btdb.defaultTimeout)
		}
	}

	if err := btdb.ReifyChat(125, 0); err != nil {
		t.Errorf("ERROR Reifying chatid %d: %v", 125, err)
		return
	}
	if err := btdb.ReifyChat(302, 0); err != nil {
		t.Errorf("ERROR Reifying chatid %d: %v", 302, err)
		return
	}

	if err := btdb.SetTimezone(302, parseTzMust("Etc/GMT+5")); err != nil {
		t.Errorf("ERROR: Setting timezone: %v", err)
		return
	}

	reminders := []struct {
		chatid       int64
		dayspecifier string
		dayoffset    string
		content      string
	}{
		{125, "daily", "7:00", "NewLife"},
		{125, "daily", "10:30", "ChristsDisciple"},
		{302, "daily", "8:30", "RelyOnGod"}, // 13:30 GMT+0
		{125, "daily", "14:00", "RelyOnGod"},
		{302, "weekdays", "9:30", "NewLife"},                // 14:30 GMT+0
		{302, "tuesday,friday", "11:00", "ChristsDisciple"}, // 16:00 GMT+0
		{125, "daily", "17:30", "ProclaimChrist"},
		{125, "daily", "21:00", "Christlikeness"},
		{125, "daily", "21:05", "Christlikeness"}, // To be deleted
	}

	for _, reminder := range reminders {
		if err := btdb.SetReminder(reminder.chatid,
			reminder.dayspecifier, reminder.dayoffset,
			reminder.content); err != nil {
			t.Errorf("ERROR Setting reminder: %v", err)
			continue
		}
	}

	if rs, err := btdb.ListReminders(125); err != nil {
		t.Errorf("ERROR listing reminders: %v", err)
	} else if rs2, err := btdb.ListReminders(302); err != nil {
		t.Errorf("ERROR listing reminders: %v", err)
	} else if len(rs)+len(rs2) != len(reminders) {
		t.Errorf("ERROR listing reminders: Expected %d got %d!",
			len(reminders), len(rs)+len(rs2))
	}

	{
		victim := len(reminders) - 1
		reminder := &reminders[victim]
		if err := btdb.DeleteReminder(reminder.chatid, reminder.dayspecifier, reminder.dayoffset); err != nil {
			t.Errorf("Deleting reminder %v: %v", *reminder, err)
		} else {
			reminders = reminders[:victim]
		}
	}

	if rs, err := btdb.ListReminders(125); err != nil {
		t.Errorf("ERROR listing reminders: %v", err)
	} else if rs2, err := btdb.ListReminders(302); err != nil {
		t.Errorf("ERROR listing reminders: %v", err)
	} else if len(rs)+len(rs2) != len(reminders) {
		t.Errorf("ERROR listing reminders: Expected %d got %d!",
			len(reminders), len(rs)+len(rs2))
	}

	scans := []struct {
		dbnow       timedb.Time
		outstanding []string
		timeout     time.Duration
	}{
		// 2020-01-15 is Wednesday
		{parseTimeMust("2020-01-15T12:00:00Z"), nil, time.Hour*1 + time.Minute*30},
		{parseTimeMust("2020-01-15T13:30:00Z"), []string{"RelyOnGod"}, time.Minute * 30},
		{parseTimeMust("2020-01-15T14:00:00Z"), []string{"RelyOnGod"}, time.Minute * 30},
		{parseTimeMust("2020-01-15T14:30:00Z"), []string{"NewLife"}, time.Hour * 3},
		{parseTimeMust("2020-01-15T17:30:00Z"), []string{"ProclaimChrist"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-15T21:00:00Z"), []string{"Christlikeness"}, time.Hour * 10},
		/* Thursday */
		{parseTimeMust("2020-01-16T07:00:00Z"), []string{"NewLife"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-16T10:30:00Z"), []string{"ChristsDisciple"}, time.Hour * 3},
		{parseTimeMust("2020-01-16T13:30:00Z"), []string{"RelyOnGod"}, time.Minute * 30},
		{parseTimeMust("2020-01-16T14:00:00Z"), []string{"RelyOnGod"}, time.Minute * 30},
		{parseTimeMust("2020-01-16T14:30:00Z"), []string{"NewLife"}, time.Hour * 3},
		{parseTimeMust("2020-01-16T17:30:00Z"), []string{"ProclaimChrist"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-16T21:00:00Z"), []string{"Christlikeness"}, time.Hour * 10},
		/* Friday */
		{parseTimeMust("2020-01-17T07:00:00Z"), []string{"NewLife"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-17T10:30:00Z"), []string{"ChristsDisciple"}, time.Hour * 3},
		{parseTimeMust("2020-01-17T13:30:00Z"), []string{"RelyOnGod"}, time.Minute * 30},
		{parseTimeMust("2020-01-17T14:00:00Z"), []string{"RelyOnGod"}, time.Minute * 30},
		{parseTimeMust("2020-01-17T14:30:00Z"), []string{"NewLife"}, time.Hour*1 + time.Minute*30},
		{parseTimeMust("2020-01-17T16:00:00Z"), []string{"ChristsDisciple"}, time.Hour*1 + time.Minute*30},
		{parseTimeMust("2020-01-17T17:30:00Z"), []string{"ProclaimChrist"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-17T21:00:00Z"), []string{"Christlikeness"}, time.Hour * 10},
		/* Saturday */
		{parseTimeMust("2020-01-18T21:00:00Z"), []string{"NewLife", "ChristsDisciple", "RelyOnGod", "RelyOnGod", "ProclaimChrist", "Christlikeness"}, time.Hour * 10},
		/* Sunday */
		{parseTimeMust("2020-01-19T07:00:00Z"), []string{"NewLife"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-19T10:30:00Z"), []string{"ChristsDisciple"}, time.Hour * 3},
		{parseTimeMust("2020-01-19T13:30:00Z"), []string{"RelyOnGod"}, time.Minute * 30},
		{parseTimeMust("2020-01-19T14:00:00Z"), []string{"RelyOnGod"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-19T17:30:00Z"), []string{"ProclaimChrist"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-19T21:00:00Z"), []string{"Christlikeness"}, time.Hour * 10},
	}

	for _, scan := range scans {
		dbnow = scan.dbnow

		t.Logf("Getting reminders at time %v", dbnow)
		if outstanding, timeout, err := btdb.RemiderScan(); err != nil {
			t.Errorf("Getting reminders at time %v: %v", dbnow, err)
		} else {
			if len(outstanding) != len(scan.outstanding) {
				t.Errorf("ERROR Expected %d outstanding reminders, got %d",
					len(scan.outstanding), len(outstanding))
			} else {
				for i := range outstanding {
					if scan.outstanding[i] != outstanding[i].Content {
						t.Errorf("ERROR Expected reminder[%d] to be %s got %s", i, scan.outstanding[i], outstanding[i].Content)
					}
				}
			}
			if timeout != scan.timeout {
				t.Errorf("ERROR Expected timout %v, got %v", scan.timeout, timeout)
			}
		}
	}

	// Move until just before the next scheduled reminder, then bump the timezone
	dbnow = parseTimeMust("2020-01-20T06:00:00Z")

	// Try changing the time zone
	if tz, err := time.LoadLocation("Australia/Sydney"); err != nil {
		t.Errorf("Error finding timezone: %v", err)
		return
	} else if err := btdb.SetTimezone(125, tz); err != nil {
		t.Errorf("Changing timezone: %v", err)
		return
	}

	// It's "now" 17:00 in Sydney due to DST (+11)
	scans = []struct {
		dbnow       timedb.Time
		outstanding []string
		timeout     time.Duration
	}{
		/* Monday */
		{parseTimeMust("2020-01-20T06:00:00Z") /* 17:00 AEDT */, nil, time.Minute * 30},
		{parseTimeMust("2020-01-20T06:30:00Z") /* 17:30 AEDT */, []string{"ProclaimChrist"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-20T10:00:00Z") /* 21:00 AEDT */, []string{"Christlikeness"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-20T13:30:00Z") /*  8:30 EST  */, []string{"RelyOnGod"}, time.Hour * 1},
		{parseTimeMust("2020-01-20T14:30:00Z") /*  9:30 EST  */, []string{"NewLife"}, time.Hour*5 + time.Minute*30},
		{parseTimeMust("2020-01-20T20:00:00Z") /*  7:00 AEDT */, []string{"NewLife"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-20T23:30:00Z") /* 10:30 AEDT */, []string{"ChristsDisciple"}, time.Hour*3 + time.Minute*30},
		/* Tuesday */
		{parseTimeMust("2020-01-21T03:00:00Z") /* 14:00 AEDT */, []string{"RelyOnGod"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-21T06:30:00Z") /* 17:30 AEDT */, []string{"ProclaimChrist"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-21T10:00:00Z") /* 21:00 AEDT */, []string{"Christlikeness"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-21T13:30:00Z") /*  8:30 EST  */, []string{"RelyOnGod"}, time.Hour * 1},
		{parseTimeMust("2020-01-21T14:30:00Z") /*  9:30 EST  */, []string{"NewLife"}, time.Hour*1 + time.Minute*30},
		{parseTimeMust("2020-01-21T16:00:00Z") /* 11:00 EST  */, []string{"ChristsDisciple"}, time.Hour * 4},
		{parseTimeMust("2020-01-21T20:00:00Z") /*  7:00 AEDT */, []string{"NewLife"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-21T23:30:00Z") /* 10:30 AEDT */, []string{"ChristsDisciple"}, time.Hour*3 + time.Minute*30},
		{parseTimeMust("2020-01-22T03:00:00Z") /* 14:00 AEDT */, []string{"RelyOnGod"}, time.Hour*3 + time.Minute*30},
	}

	for _, scan := range scans {
		dbnow = scan.dbnow

		t.Logf("Getting reminders at time %v", dbnow)
		if outstanding, timeout, err := btdb.RemiderScan(); err != nil {
			t.Errorf("Getting reminders at time %v: %v", dbnow, err)
		} else {
			if len(outstanding) != len(scan.outstanding) {
				t.Errorf("ERROR Expected %d outstanding reminders, got %d",
					len(scan.outstanding), len(outstanding))
			} else {
				for i := range outstanding {
					if scan.outstanding[i] != outstanding[i].Content {
						t.Errorf("ERROR Expected reminder[%d] to be %s got %s", i, scan.outstanding[i], outstanding[i].Content)
					}
				}
			}
			if timeout != scan.timeout {
				t.Errorf("ERROR Expected timout %v, got %v", scan.timeout, timeout)
			}
		}
	}

}
