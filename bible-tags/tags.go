package bibletags

import (
	_ "embed"
	"fmt"
	"math/rand"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/martyros/sqlutil/txutil"
)

/* Ezra Bible Bot
Copyright (C) 2022  George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

func init() {
	rand.Seed(time.Now().UnixNano())
}

func (btdb *BibleTagDb) NewTag(chatid int64, tagname string) error {
	if _, err := btdb.db.Exec(
		`insert into tags(tagname, chatid) values(?, ?)`,
		tagname, chatid); err != nil {
		return fmt.Errorf("Inserting new tag (%s, %d): %w",
			tagname, chatid, err)
	}

	return nil
}

type ListTagResult struct {
	Tagname  string
	IsGlobal bool
}

// ListTags lists tags available to user `chatid`.  This will include both that
// user's local tags, as well as the global tags.  If the chatid doesnt' exist,
// all global tags will be listed.
func (btdb *BibleTagDb) ListTags(chatid int64) ([]ListTagResult, error) {
	var results []ListTagResult

	if err := btdb.db.Select(
		&results,
		`select tagname, (chatid=?) as isglobal
		  from tags
		  where chatid in (?, ?)
		 order by isglobal, tagname`,
		CHATID_GLOBAL, CHATID_GLOBAL, chatid); err != nil {
		return nil, fmt.Errorf("Getting tag list for chat %d: %w", chatid, err)
	}

	return results, nil
}

// DeleteTag will delete the given tag name, and all passage tags, for the given
// chatid. Global tags can be deleted by passing CHATID_GLOBAL.
func (btdb *BibleTagDb) DeleteTag(chatid int64, tagname string) error {
	return txutil.TxLoopDb(btdb.db, func(eq sqlx.Ext) error {
		if _, err := eq.Exec(
			`update viewlog set tagchatid=NULL, tagname=NULL 
				where tagchatid=? and tagname=?`, chatid, tagname); err != nil {
			return fmt.Errorf("Replacing tag references with NULL: %w", err)
		}

		if _, err := eq.Exec(`delete from passagetags where tagname=? and chatid=?`,
			tagname, chatid); err != nil {
			return fmt.Errorf("Removing tag %s for chat %d from passages: %w", tagname, chatid, err)
		}

		if result, err := eq.Exec(`delete from tags where tagname=? and chatid=?`,
			tagname, chatid); err != nil {
			return fmt.Errorf("Deleting tag %s for chat %d: %w", tagname, chatid, err)
		} else if affected, err := result.RowsAffected(); err != nil {
			return fmt.Errorf("Database error getting rows affected: %w", err)
		} else if affected == 0 {
			return fmt.Errorf("Couldn't find tag to delete")
		}
		return nil
	})
}

func (btdb *BibleTagDb) TagPassage(chatid int64, tagname string, passage string) error {
	if _, err := btdb.db.Exec(
		`insert into passagetags(tagname, chatid, passage) values(?, ?, ?)`,
		tagname, chatid, passage); err != nil {
		return fmt.Errorf("Tagging passage: %w", err)
	}
	return nil
}

func getTagCount(eq sqlx.Queryer, chatid int64, tagname string) (int, error) {
	var tagcount int
	err := sqlx.Get(eq,
		&tagcount,
		`select count(*) from tags where chatid in (?, ?) and tagname=?`,
		CHATID_GLOBAL, chatid, tagname)
	return tagcount, err
}

type ShowTagResult struct {
	Passage  string
	IsGlobal bool
}

// ShowTagPassages lists all passages with a given tag visible to the given
// chatid; that is, either that chatid's own tag, or a global tag of the same
// name.  Will return success with an empty result if the tag exists in either
// place but neither has any tags.  Will return failure if the tag doesn't exist
// in either place.
func (btdb *BibleTagDb) ShowTagPassages(chatid int64, tagname string) ([]ShowTagResult, error) {
	var results []ShowTagResult

	err := txutil.TxLoopDb(btdb.db, func(eq sqlx.Ext) error {
		if tagcount, err := getTagCount(eq, chatid, tagname); err != nil {
			return fmt.Errorf("Checking for existence of tag (%d, %s): %w", chatid, tagname, err)
		} else if tagcount == 0 {
			return fmt.Errorf("No tag %s found", tagname)
		}
		if err := sqlx.Select(eq,
			&results,
			`select passage, (chatid=?) as isglobal
				from passagetags
				where tagname=? and chatid in (?, ?)
				order by isglobal, passage`,
			CHATID_GLOBAL, tagname, chatid, CHATID_GLOBAL); err != nil {
			return fmt.Errorf("Getting passage list for tag %s user %d: %w", tagname, chatid, err)
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return results, nil
}

// UntagPassage will remove a tag from the given passage.  This will fail if the
// tag doesn't exist (including if the chatid in question doesn't exist), or if
// the passage was not tagged with the given tag.
func (btdb *BibleTagDb) UntagPassage(chatid int64, tagname string, passage string) error {
	return txutil.TxLoopDb(btdb.db, func(eq sqlx.Ext) error {
		if tagcount, err := getTagCount(eq, chatid, tagname); err != nil {
			return fmt.Errorf("Checking for existence of tag (%d, %s): %w", chatid, tagname, err)
		} else if tagcount == 0 {
			return fmt.Errorf("No tag %s found", tagname)
		}

		if result, err := eq.Exec(
			`delete from passagetags where tagname=? and chatid=? and passage=?`,
			tagname, chatid, passage); err != nil {
			return fmt.Errorf("Untagging chat %d tag %s from passage %s: %w",
				chatid, tagname, passage, err)
		} else if affected, err := result.RowsAffected(); err != nil {
			return fmt.Errorf("Database error getting rows affected: %w", err)
		} else if affected == 0 {
			return fmt.Errorf("Passage not tagged")
		}

		return nil
	})
}

//go:embed query-passage-history.sql
var queryGetTag string

// GetTag is really the whole point of the library: Given a tagname and a chat
// id, return a passage from the given tag, randomly, weighted by length of time
// since being seen (up to one year).  Will return an error
func (btdb *BibleTagDb) GetTag(chatid int64, tagname string) (string, error) {
	var passage string

	err := txutil.TxLoopDb(btdb.db, func(eq sqlx.Ext) error {
		var result []struct {
			Passage    string
			SecondsAgo int
		}

		if tagcount, err := getTagCount(eq, chatid, tagname); err != nil {
			return fmt.Errorf("Checking for existence of tag (%d, %s): %w", chatid, tagname, err)
		} else if tagcount == 0 {
			return fmt.Errorf("No tag %s found", tagname)
		}

		if err := sqlx.Select(eq, &result, queryGetTag, tagname, CHATID_GLOBAL, chatid, chatid); err != nil {
			return fmt.Errorf("Getting passage view stamps for chatid %d tagname %s: %w", chatid, tagname, err)
		}

		if len(result) < 1 {
			return fmt.Errorf("No passages with tag %s", tagname)
		}

		total := 0
		for i := range result {
			total += result[i].SecondsAgo
		}

		rval := rand.Intn(total)

		for i := range result {
			total -= result[i].SecondsAgo
			if total <= rval {
				passage = result[i].Passage
				break
			}
		}

		if passage == "" {
			return fmt.Errorf("INTERNAL ERROR: No passage chosen")
		}

		if _, err := eq.Exec(
			`insert into viewlog(viewchatid, passage, ts) values (?, ?, datetime())`,
			chatid, passage); err != nil {
			return fmt.Errorf("Inserting viewlog: %w", err)
		}

		return nil
	})

	return passage, err
}
